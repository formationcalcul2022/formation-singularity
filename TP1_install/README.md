# TP1, installation sur DEBIAN 11 de singularity 3.9.2
---

Sur un hôte Linux installé en DEBIAN 11, nous allons installer Singularity 3.9.2.

Pour cela, on va copier le fichier https://forgemia.inra.fr/formationcalcul2022/formation-singularity/-/blob/master/TP1_install/singularity_intall.sh contenant toutes les étapes d'installation et l'éxecuter

```bash
ssh user@195.221.108.97
wget https://forgemia.inra.fr/formationcalcul2022/formation-singularity/-/blob/master/TP1_install/singularity_intall.sh
bash singularity_intall.sh
```
Voici le contenu du  fichier =>
```bash
#! /usr/bin/env bash
# Installation de singularity-ce en 3.9.2
set -e 

sudo apt-get update && sudo apt-get install -y \ 
    build-essential \ 
    uuid-dev \ 
    libgpgme-dev \ 
    squashfs-tools \ 
    libseccomp-dev \ 
    wget \ 
    pkg-config \ 
    git \ 
    cryptsetup-bin

export VERSION=1.17.6 OS=linux ARCH=amd64 && \ 
    wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz && \ 
    sudo tar -C /usr/local -xzvf go$VERSION.$OS-$ARCH.tar.gz && \ 
    rm go$VERSION.$OS-$ARCH.tar.gz

echo 'export GOPATH=${HOME}/go' >> ~/.bashrc && \ 
    echo 'export PATH=/usr/local/go/bin:${PATH}:${GOPATH}/bin' >> ~/.bashrc && \ 
    source ~/.bashrc

export VERSION=3.9.2 && # adjust this as necessary \ 
    wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-ce-${VERSION}.tar.gz && \ 
    tar -xzf singularity-ce-${VERSION}.tar.gz && \ 
    cd singularity-ce-${VERSION}

./mconfig && \ 
    make -C ./builddir && \ 
    sudo make -C ./builddir install

echo ". /usr/local/etc/bash_completion.d/singularity" >> $HOME/.bashrc
```

Voici le déroule du script:
* Installation des outils systèmes nécessaires à Singularity et le language GO.
* Installation de GO sur l'hôte avec une version spécifique (ici 1.17.6)
* Installation de  Singularity (compilation avec make)
* Ajout de la completion Singularity dans le bashrc de l'utilisateur
