[.left]
image::../../img/singularity.png[]
:icons: font

<<<
= Making an OpenMPI container

In the data directory of the tutorial repository, you'll find a file named mpi-ping.c. It's a source for a basic open-mpi ping program.

Let's build a container with compilation tools, OpenMPI env and source code inside. Then compile the source code inside the container.

==== Sources are in the folder TP5

_openmpi.def_ :
----
Bootstrap: docker
From:debian:9.8

%files
    mpitest.c /opt

%environment
    export OMPI_DIR=/opt/ompi
    export SINGULARITY_OMPI_DIR=$OMPI_DIR
    export SINGULARITYENV_APPEND_PATH=$OMPI_DIR/bin
    export SINGULAIRTYENV_APPEND_LD_LIBRARY_PATH=$OMPI_DIR/lib

%post
    echo "Installing required packages..."
    apt-get update && apt-get install -y bzip2 wget git bash gcc gfortran g++ make file

    echo "Installing Open MPI"
    export OMPI_DIR=/opt/ompi
    export OMPI_VERSION=2.1.2
    export OMPI_URL="https://download.open-mpi.org/release/open-mpi/v2.1/openmpi-$OMPI_VERSION.tar.bz2"
    mkdir -p /opt/ompi
    mkdir -p /opt
    # Download
    cd /opt/ompi && wget -O openmpi-$OMPI_VERSION.tar.bz2 $OMPI_URL && tar -xjf openmpi-$OMPI_VERSION.tar.bz2
    # Compile and install
    cd /opt/ompi/openmpi-$OMPI_VERSION && ./configure --prefix=$OMPI_DIR && make install
    # Set env variables so we can compile our application
    export PATH=$OMPI_DIR/bin:$PATH
    export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
    export MANPATH=$OMPI_DIR/share/man:$MANPATH

    echo "Compiling the MPI application..."
    cd /opt && mpicc -o mpitest mpitest.c
----


In your folder, you should have a "mpitest" executable. In that case, you have compiled your file outside the container. You can also compile it inside, to bring the compiled file with your container.

Test your compiled file inside your container in a SHELL :
[source, bash]
singularity shell openmpi.sif

[source, bash]
/opt/ompi/bin/mpirun /opt/mpitest

If you have a server with an mpi installed you can test it outside your container (note that you have to use the mpirun from the host, because mpi use the hardware infrastructure to dispatch program) :
[source, bash]
mpirun -n 5 singularity exec openmpi.sif /opt/mpitest

WARNING: The OpenMPI versions inside and outside the container can different but must be compatible to work together.

== Working on Genotoul cluster

Try to connect to the Genotoul cluster: footnote:[Thanks to the Genotoul platform.]:
[source, bash]
----
ssh @muse-login.hpc-lr.univ-montp2.fr
cd work
----

You can now upload your OpenMPI container and run it on the cluster.

Then load the singularity and openmpi env :
[source, bash]
----
#module available
module load system/singularity-3.5.3
module load compiler/gcc-6.4.0
module load mpi/openmpi-2.1.2
#module list
singularity help
----

If everything's OK, you could now run openmpi singularity images :
[source, bash]
mpirun -n 5 singularity exec -B /work/<mylogin> mpideb.sif /opt/mpitest

You can submit in interactive mode with the following command :

[source, bash]
srun -p singularity mpirun singularity exec mpi.sif ./a.out

Or in batch mode, with the following submission script :

_runopenmpi.sh_
----
#!/bin/sh
#SBATCH --job-name=test
#SBATCH -N 5
#SBATCH --ntasks-per-node=5
#SBATCH --time=01:00:00

module load system/singularity-3.5.3
module load compiler/gcc-6.4.0
module load mpi/openmpi-2.1.2

echo « Running on: $SLURM_NODELIST »

mpirun singularity exec mpi.sif /opt/mpitest
----

Use sbatch to run it :

[source, bash]
sbatch runopenmpi.sh
<<<
