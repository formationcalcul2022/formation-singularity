[.left]
image:./img/singularity.png[]

:icons: font

<<<

[source, bash]
git clone https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity.git


= Introduction

Singularity is a container solution created by necessity for scientific and application driven workloads.

* Singularity containers utilize a single file which is the complete representation of all the files within the container. The same features which facilitate mobility also facilitate reproducibility. Once a contained workflow has been defined, a snapshot image can be taken of a container. The image can be then archived and locked down such that it can be used later and you can be confident that the code within the container has not changed.

* Mobility of compute is defined as the ability to define, create and maintain a workflow and be confident that the workflow can be executed on different hosts, operating systems (as long as it is Linux) and service providers.

* Singularity can give the user the freedom they need to install the applications, versions, and dependencies for their workflows without impacting the system in any way. Users can define their own working environment and literally copy that environment image (single file) to a shared resource and run their workflow inside that image.

* Singularity natively supports InfiniBand, Lustre, and works seamlessly with all resource managers (e.g. SLURM, Torque, SGE, etc.) because it works like running any other command on the system. It also has built-in support for MPI and for containers that need to leverage GPU resources.

* A user inside a Singularity container is the same user as outside the container. To mitigate security concerns, Singularity limits one’s ability to escalate permission inside a container. For example, if I do not have root access on the target system, I should not be able to escalate my privileges within the container to root either.

* The goals of Singularity are mobility, reproducibility and freedom, not full isolation (as you would expect from industry-driven container technologies): Singularity only separates the needed namespaces in order to satisfy our primary goals. Coupling incomplete isolation with the fact that a user inside a container is the same user outside the container, allows Singularity to blur the lines between a container and the underlying host system.

* In many circumstances building containers require root administrative privileges just like these actions would require on any system, container, or virtual machine. This means that a user must have access to a system on which they have root privileges. This could be a server, a workstation, a laptop, a virtual machine, or even a cloud instance. Once you have the container with the necessary applications, libraries and data inside it can be easily shared with other hosts and executed without requiring root access.


==== A Singularity container is just an enhanced binary file!


= Sinngularity Help:

* https://sylabs.io/guides/latest/user-guide/
* https://groups.google.com/a/lbl.gov/forum/#!forum/singularity
* https://team.forgemia.inra.fr/containers

= More informations on container comparative stud:
DOCKER VS SINGULARITY VS SHIFTER VS UGE CONTAINER EDITION:
https://tin6150.github.io/psg/blogger_container_hpc.html

