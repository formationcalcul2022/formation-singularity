# R Singularity container
## R MKL Version: 3.6.3

### CI/CD concepts
With the continuous method of software development, you continuously build, test, and deploy iterative code changes. This iterative process helps reduce the chance that you develop new code based on buggy or failed previous versions. With this method, you strive to have less human intervention or even no intervention at all, from the development of new code until its deployment.

The three primary approaches for the continuous method are:

- Continuous Integration
- Continuous Delivery
- Continuous Deployment

#### ci/cd tutorial:
GitLab CI/CD can automatically build, test, deploy, and monitor your applications by using Auto DevOps.

For a complete overview of these methodologies and GitLab CI/CD, read the Introduction to CI/CD with GitLab.

https://docs.gitlab.com/ee/ci/introduction/index.html
https://docs.gitlab.com/ee/ci/quick_start/

#### - ci/cd templates:
https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates




## R-mkl Singularity container
R 3.6 with BLAS MKL<br>
Singularity container based on the recipe: Singularity.R.3.6.3_mkl.def


image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```bash
singularity pull R.3.6.3_mkl.sif oras://registry.forgemia.inra.fr/pepi-atelier-singularity/r_mkl/r_mkl:latest
```



### To build locally the image:

```bash
sudo singularity build R.3.6.3_mkl.sif Singularity.R.3.6.3_mkl.def
```



