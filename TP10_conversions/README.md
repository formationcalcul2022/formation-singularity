## Convert docker <-> singularity recipes

###You can transform a Dockerfile into a singularity recipe or vise-versa using Singularity Python. Singularity Python offers some very helpful utilities, consider to install it if you plan to work with singularity a lot

https://sylabs.io/guides/2.6/user-guide/singularity_and_docker.html

#### spython or pull a container as described in Singularity Python install:
https://singularityhub.github.io/singularity-cli/install

Source code:

https://github.com/singularityhub/singularity-cli

#### Find more about recipe conversion here
https://singularityhub.github.io/singularity-cli/recipes

```bash
pip3 install spython # if you do not have spython install it from the command line
```

```bash
# print in the console
spython recipe Dockerfile

# save in the *.def file
spython recipe Dockerfile &> Singularity.def
```

