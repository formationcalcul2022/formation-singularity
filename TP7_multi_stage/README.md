# Sigularity multi-stage build

#### https://sylabs.io/guides/latest/user-guide/definition_files.html#multi-stage-builds

122M Mar  4 10:25 hellogo_full.sif

3.6M Mar  4 10:23 hellogo.sif


## 2) Singularity container based on the recipe: Singularity.R.3.6.3.def

- build locally the image:

```bash
sudo singularity build R.3.6.3.sif Singularity.R.3.6.3.def
```

docs sur conda pack:
https://conda.github.io/conda-pack/

